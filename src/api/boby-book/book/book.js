import request from '@/utils/request'


export function getPage(query) {
    return request({
        url: '/boby_book/book/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/boby_book/book/',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/boby_book/book/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/boby_book/book/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/boby_book/book',
        method: 'put',
        data: obj
    })
}
