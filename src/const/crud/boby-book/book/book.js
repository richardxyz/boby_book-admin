export const tableOption = {
    dialogDrag: true,
    border: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    menuType: 'text',
    searchShow: false,
    excelBtn: false,
    printBtn: false,
    viewBtn: true,
    column: [
        {
            label: '主键',
            prop: 'id',
            sortable: false,
            display: false,
            hide: true
        },
        {
            label: '封面',
            prop: 'bookCover',
            type: 'upload',
            listType: 'picture-img',
            propsHttp: {
                res: 'data'
            },
            canvasOption: {
                text: 'avue',
                ratio: 0.1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
        },
        {
            label: '名称',
            prop: 'bookName',
            sortable: false
        },
        {
            label: '作者',
            prop: 'bookAuthor',
            sortable: false
        },
        {
            label: '简介',
            prop: 'bookSynopsis',
            sortable: false,
            display: false,
            hide: true
        },
        {
            label: '类别',
            prop: 'bookCategory',
            sortable: false,
            type: 'select',
            dicUrl: 'http://localhost:8088/renren-admin/boby_book/bookcategory/getCategoryAll',
            props: {
                label: 'categoryName',
                value: 'id'
            }
        },
        {
            label: '章节数',
            prop: 'bookChapterNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '订阅数',
            prop: 'bookSubscribeNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '播放数',
            prop: 'bookPlayNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '收藏数',
            prop: 'bookCollectionNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '评论数',
            prop: 'bookCommentNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '点赞数',
            prop: 'bookFabulousNum',
            sortable: false,
            type: 'number',
            minRows: 0
        },
        {
            label: '评分',
            prop: 'score',
            sortable: false,
            type: 'rate',
            colors: ['#99A9BF', '#F7BA2A', '#FF9900'],
            slot: true
        },
        {
            label: '上架时间',
            prop: 'shelfTime',
            sortable: false,
            type: 'datetime',
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
        },
        {
            label: '更新者',
            prop: 'updater',
            sortable: false,
            display: false,
            hide: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            sortable: false,
            display: false,
            hide: true
        }
    ]
}
